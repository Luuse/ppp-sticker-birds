import scribus
import glob

slas = glob.glob('sla/*.sla')

for i, file in enumerate(slas):
    nm = file.split('.')[1]
    print(nm)
    scribus.openDoc(file)
    pdf = scribus.PDFfile()
    pdf.file = 'pdf/'+nm+'-'+str(i)+'.pdf'
    pdf.save()
