from jinja2 import Template
import requests
import scribus
import random
import sys




def getBirds():
    birds_list = []
    for i in range(30):
        p = i + 1
        try:
            response = requests.get("https://xeno-canto.org/api/2/recordings?query=cnt:"+sys.argv[1]+"&page="+str(p))
            re = response.json()
            for bird in re['recordings']:
                if bird['gen'][0] == "P":
                    if not bird['gen'] in birds_list:
                        birds_list.append(bird['gen'])
        except:
            print('error')

    print(birds_list)
    return birds_list


with open('sticker.sla') as f:
    tmpl = Template(f.read())


data = getBirds() 


for i in range(3):
    random.shuffle(data)
    content = tmpl.render(data=data)
    outScribus = open("sla/out-"+str(i)+".sla", "w")
    outScribus.write(content)
    outScribus.close()

print(sys.argv[1])
